import React, { Component } from 'react';
import { View, StyleSheet, ImageBackground } from 'react-native';
import { Container } from 'native-base';
import BottomSheet from './src/component/BottomSheet';
import Item from './src/component/Item';

export default class SlidingApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [
                { arrayValues: ["فروردین", "اردیبهشت", "خرداد"], selectedValue: "", placeHolder: "بهار ... " },
                { arrayValues: ["تیر", "مرداد", "شهریور"], selectedValue: "", placeHolder: "تابستان ... " },
                { arrayValues: ["مهر", "آبان", "آذر"], selectedValue: "", placeHolder: "پاییز ... " },
                { arrayValues: ["دی", "بهمن", "اسفند"], selectedValue: "", placeHolder: "زمستان ... " }
            ],
            selectedItemIndex: null,
            arrayValues: [],
            selectedValueForThisItem: "",
            showBottomSheet: false,
        };
    }
    _openBottomSheet(id) {
        this.setState({
            arrayValues: this.state.data[id].arrayValues,
            showBottomSheet: true,
            selectedItemIndex: id,
            selectedValueForThisItem: this.state.data[id].selectedValue,
        })
    }

    _selectedValue(value) {
        var newData = []
        this.state.data.map((rowData, index) => {
            if (index == this.state.selectedItemIndex) {
                newData.push({
                    arrayValues: rowData.arrayValues,
                    selectedValue: value,
                    placeHolder: rowData.placeHolder,
                })
            } else {
                newData.push(rowData)
            }
        })
        this.setState({
            data: newData,
            showBottomSheet: false
        })
    }


    render() {
        var items = [];
        for (let i = 0; i < this.state.data.length; i++) {
            items.push(
                <Item
                    placeHolder={this.state.data[i].placeHolder}
                    value={this.state.data[i].selectedValue}
                    id={i}
                    itemSelected={() => {
                        this._openBottomSheet(i)
                    }}

                />
            );
        }

        return (
            <Container>
                <ImageBackground source={require("./asset/images/back.png")}
                    resizeMode={"contain"}
                    style={{ flex: 1, backgroundColor: "#eee" }}
                    imageStyle={{ opacity: 1 }}>
                    <View style={styles.container}>
                        <View style={styles.items}>
                            {items}
                        </View>
                    </View>
                    <BottomSheet
                        showBottomSheet={this.state.showBottomSheet}
                        data={this.state.arrayValues}
                        value={this.state.selectedValueForThisItem}
                        selectedValue={(item) => {
                            this._selectedValue(item)
                        }}
                    />
                </ImageBackground>
            </Container >

        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        flex: 1,
    },
    items: {
        marginTop: "30%",
        padding: 25,
        paddingTop: 33,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "center",
        backgroundColor: "#FFF",
        borderColor: "#ccc",
        borderWidth: 1,
        borderRadius: 10,
    },


});

