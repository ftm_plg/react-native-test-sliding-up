import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'native-base';

export default class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            value: "",
            placeHolder: ""
        };
    }

    componentDidMount() {
        this.setState({
            value: this.props.value,
            placeHolder: this.props.placeHolder
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            value: nextProps.value,
            placeHolder: nextProps.placeHolder
        })

    }

    render() {

        return (
            <TouchableOpacity onPress={() => {
                this.props.itemSelected()
            }}>
                <View style={styles.container}>
                    <Text style={styles.itemText}>
                        {this.state.value ? this.state.value : this.props.placeHolder}
                    </Text>
                    <View style={styles.itemIcon}>
                        <Icon type="FontAwesome" name="plus-square-o" style={styles.icon} />
                    </View>
                </View>
            </TouchableOpacity>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        borderColor: "#ccc",
        borderWidth: 1,
        borderRadius: 12,
        width: "70%",
        justifyContent: "space-between",
        padding: 5,
        marginBottom: 8
    },
    itemText: {
        flex: 7,
        textAlign: "right",
        padding: 15,
        paddingRight: 8,
        justifyContent: "center",
        textAlignVertical:"center"
    },
    itemIcon: {
        flex: 3,
        justifyContent: "center",
        alignSelf:"center"
    },
    icon: {
        fontSize: 18,
        textAlign: "center",
        color: "gray"
    }
});

