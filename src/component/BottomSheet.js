import React, { Component } from 'react';
import { View, StyleSheet, Text, Animated, PanResponder, FlatList, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';

const valueOfSheet = 1000
const Item = ({ title, myThis }) => (
    <TouchableOpacity
        onPress={() => {
            myThis.props.selectedValue(title) ||
                myThis._close()

        }}
        style={styles.item}>
        <View style={{ flexDirection: "row", justifyContent: "flex-end" }}>
            <Text style={{ textAlign: 'right', fontSize: 16, color: myThis.state.value == title ? "green" : "black", paddingRight: 5 }}> {title} </Text>
            {myThis.state.value == title ? <Icon type="FontAwesome" name="check" style={styles.icon} /> : <View></View>}

        </View>

    </TouchableOpacity>
);

export default class SlidingApp extends Component {
    constructor(props) {
        super(props);
        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: (event, gestureState) => {
                this.state.bounceValue.setValue(Math.max(0, 0 + gestureState.dy));
            },
            onPanResponderRelease: (e, gesture) => {
                const shouldOpen = gesture.vy <= 0;
                Animated.spring(this.state.bounceValue, {
                    toValue: shouldOpen ? 0 : valueOfSheet,
                    velocity: gesture.vy,
                    tension: 2,
                    friction: 8,
                }).start();
            },
        });



        this.state = {
            bounceValue: new Animated.Value(valueOfSheet),
            show: false,
            data: [],
            value: ""
        };
    }

    componentDidMount() {
        this.setState({
            show: this.props.showBottomSheet,
            data: this.props.data,
            value: this.props.value
        })
        if (this.props.showBottomSheet) {
            this._open()
        } else {
            this._close()
        }

    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            show: nextProps.showBottomSheet,
            data: nextProps.data,
            value: nextProps.value
        })
        if (nextProps.showBottomSheet) {
            this._open()
        } else {
            this._close()
        }
    }

    _open() {
        Animated.spring(
            this.state.bounceValue,
            {
                toValue: 0,
                velocity: 3,
                tension: 2,
                friction: 8,
            }
        ).start();
    }

    _close() {
        Animated.spring(this.state.bounceValue, {
            toValue: valueOfSheet,
            velocity: 3,
            tension: 2,
            friction: 8,
        }).start();
    }

    render() {
        return (
            <View style={styles.container}>
                <Animated.View
                    {...this.panResponder.panHandlers}
                    style={[styles.subView,
                    { transform: [{ translateY: this.state.bounceValue }] }]

                    }
                >
                    <FlatList
                        data={this.state.data}
                        renderItem={({ item }) => <Item title={item} myThis={this} />}
                    />

                </Animated.View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    subView: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "#FFF",
        padding: 25,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderWidth: 1,
        borderColor: "#ccc"
    },
    item: {
        borderBottomColor: "#eee",
        borderBottomWidth: 1,
        padding: 8,
    },
    icon: {
        fontSize: 18,
        textAlign: "center",
        color: "green"
    }
});

