/**
 * @format
 */

import 'react-native-gesture-handler';
import {AppRegistry, I18nManager} from 'react-native';
import {name as appName} from './app.json';
import SlidingApp from './SlidingApp';

console.disableYellowBox = true;
I18nManager.forceRTL(false);
I18nManager.allowRTL(false);

AppRegistry.registerComponent(appName, () => SlidingApp);
